﻿using CASPER.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CASPER.Infrastructure.Persistence.Configurations;

public class LegalEntityConfiguration : IEntityTypeConfiguration<LegalEntity>
{
    public void Configure(EntityTypeBuilder<LegalEntity> builder)
    {
        builder.Property(e => e.Name)
            .HasMaxLength(80)
            .IsRequired();

        builder.HasOne(e => e.VirtualAssetServiceProvider)
            .WithMany(serviceProvider => serviceProvider.LegalEntities);

        builder.HasOne(e => e.PaymentProvider)
            .WithMany(serviceProvider => serviceProvider.LegalEntities);
    }
}
