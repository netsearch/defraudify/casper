﻿using CASPER.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CASPER.Infrastructure.Persistence.Configurations;

public class VirtualAssetServiceProviderPropertyConfiguration
    : IEntityTypeConfiguration<VirtualAssetServiceProviderProperty>
{
    public void Configure(EntityTypeBuilder<VirtualAssetServiceProviderProperty> builder)
    {
        builder.HasKey(e => new {e.VirtualAssetServiceProviderId, e.Name});

        builder.Property(e => e.Name)
            .HasMaxLength(60)
            .IsRequired();

        builder.Property(e => e.Content)
            .HasColumnType("json")
            .HasMaxLength(300)
            .IsRequired(false);

        builder.HasOne(e => e.VirtualAssetServiceProvider)
            .WithMany(serviceProvider => serviceProvider.Properties)
            .IsRequired();
    }
}
