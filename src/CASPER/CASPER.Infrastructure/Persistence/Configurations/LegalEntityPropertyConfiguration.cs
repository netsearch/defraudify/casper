﻿using CASPER.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CASPER.Infrastructure.Persistence.Configurations;

public class LegalEntityPropertyConfiguration : IEntityTypeConfiguration<LegalEntityProperty>
{
    public void Configure(EntityTypeBuilder<LegalEntityProperty> builder)
    {
        builder.HasKey(e => new {e.LegalEntityId, e.Name});

        builder.Property(e => e.Name)
            .HasMaxLength(60)
            .IsRequired();

        builder.Property(e => e.Content)
            .HasColumnType("json")
            .HasMaxLength(300)
            .IsRequired(false);

        builder.HasOne(e => e.LegalEntity)
            .WithMany(legalEntity => legalEntity.Properties)
            .IsRequired();
    }
}
