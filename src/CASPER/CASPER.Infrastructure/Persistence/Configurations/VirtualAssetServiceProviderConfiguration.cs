﻿using CASPER.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CASPER.Infrastructure.Persistence.Configurations;

public class VirtualAssetServiceProviderConfiguration : IEntityTypeConfiguration<VirtualAssetServiceProvider>
{
    public void Configure(EntityTypeBuilder<VirtualAssetServiceProvider> builder)
    {
        builder.Property(e => e.Name)
            .HasMaxLength(60)
            .IsRequired();

        builder.Property(e => e.Website)
            .HasMaxLength(120)
            .IsRequired();

        builder.HasIndex(e => e.Website)
            .IsUnique();

        builder.Property(e => e.Address)
            .HasMaxLength(300)
            .IsRequired();
    }
}
