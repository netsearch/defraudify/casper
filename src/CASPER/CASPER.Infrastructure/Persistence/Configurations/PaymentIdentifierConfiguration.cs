﻿using CASPER.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CASPER.Infrastructure.Persistence.Configurations;

public class PaymentIdentifierConfiguration : IEntityTypeConfiguration<PaymentIdentifier>
{
    public void Configure(EntityTypeBuilder<PaymentIdentifier> builder)
    {
        builder.Property(e => e.IBAN)
            .HasMaxLength(34)
            .IsRequired();

        builder.HasOne(e => e.PaymentProvider)
            .WithMany(paymentProvider => paymentProvider.PaymentIdentifiers)
            .IsRequired(false);

        builder.HasOne(e => e.VirtualAssetServiceProvider)
            .WithMany(vasp => vasp.PaymentIdentifiers)
            .IsRequired(false);
    }
}
