using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace CASPER.Infrastructure.Persistence;

public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
{
    public ApplicationDbContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
        optionsBuilder.UseNpgsql("Server=127.0.0.1;Port=5432;Database=CASPER;UserId=CASPER;Password=CASPER"
                                 + ";IncludeErrorDetail=true;");

        return new ApplicationDbContext(optionsBuilder.Options, null!);
    }
}
