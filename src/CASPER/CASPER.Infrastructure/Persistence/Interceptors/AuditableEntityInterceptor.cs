using CASPER.Domain.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace CASPER.Infrastructure.Persistence.Interceptors;

public class AuditableEntityInterceptor : SaveChangesInterceptor
{
    public override InterceptionResult<int> SavingChanges(DbContextEventData eventData, InterceptionResult<int> result)
    {
        if (eventData.Context is not null)
        {
            ProcessAuditableEntities(eventData.Context);
        }
        return base.SavingChanges(eventData, result);
    }

    public override async ValueTask<InterceptionResult<int>> SavingChangesAsync(
        DbContextEventData eventData,
        InterceptionResult<int> result,
        CancellationToken cancellationToken = new()
    )
    {
        if (eventData.Context is not null)
        {
            ProcessAuditableEntities(eventData.Context);
        }
        return await base.SavingChangesAsync(eventData, result, cancellationToken);
    }

    private void ProcessAuditableEntities(DbContext eventDataContext)
    {
        foreach (var entityEntry in eventDataContext.ChangeTracker.Entries<AuditableEntityBase>())
        {
            if (entityEntry.State == EntityState.Modified)
            {
                entityEntry.Entity.LastUpdatedAt = DateTime.UtcNow;
            }
        }
    }
}
