﻿using System.Reflection;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Common;
using CASPER.Domain.Entities;
using CASPER.Domain.Enums;
using CASPER.Domain.ValueObjects;
using CASPER.Infrastructure.Common;
using CASPER.Infrastructure.Converters;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Infrastructure.Persistence;

public class ApplicationDbContext : DbContext, IApplicationDbContext
{
    private readonly IMediator _mediator;

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IMediator mediator) : base(options)
    {
        _mediator = mediator;
    }

    public DbSet<TEntity> GetDbSet<TEntity>() where TEntity : EntityBase
        => Set<TEntity>();

    public DbSet<LegalEntity> LegalEntities
        => Set<LegalEntity>();

    public DbSet<LegalEntityProperty> LegalEntityProperties
        => Set<LegalEntityProperty>();

    public DbSet<PaymentProvider> PaymentProviders
        => Set<PaymentProvider>();

    public DbSet<PaymentIdentifier> PaymentIdentifiers
        => Set<PaymentIdentifier>();

    public DbSet<VirtualAssetServiceProvider> VirtualAssetServiceProviders
        => Set<VirtualAssetServiceProvider>();

    public DbSet<VirtualAssetServiceProviderProperty> VirtualAssetServiceProviderProperties
        => Set<VirtualAssetServiceProviderProperty>();

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        await _mediator.DispatchDomainEvents(this);
        
        return await base.SaveChangesAsync(cancellationToken);
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        builder.HasPostgresEnum<BusinessServices>();
        builder.HasPostgresEnum<TechnologyServices>();
        
        foreach (var mutableEntityType in builder.Model.GetEntityTypes())
        {
            builder
                .Entity(mutableEntityType.Name)
                .Property<DateTime>("CreatedAt")
                .HasDefaultValueSql("NOW() AT TIME ZONE 'UTC'")
                .ValueGeneratedOnAdd()
                .IsRequired();

            builder
                .Entity(mutableEntityType.Name)
                .Property<DateTime?>("LastUpdatedAt")
                .ValueGeneratedOnUpdate()
                .IsRequired(false);
        }

        base.OnModelCreating(builder);
    }

    protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
    {
        configurationBuilder.Properties<CountryIdentifier>()
            .HaveConversion<CountryIdentifierConverter>()
            .HaveMaxLength(2)
            .AreUnicode(false);

        configurationBuilder.Properties<IBAN>()
            .HaveConversion<string>()
            .HaveMaxLength(40)
            .AreUnicode(false);

        base.ConfigureConventions(configurationBuilder);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
    }
}
