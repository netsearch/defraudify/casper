﻿using System.Reflection;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Enums;
using CASPER.Infrastructure.Identity;
using CASPER.Infrastructure.Persistence;
using CASPER.Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;

namespace CASPER.Infrastructure;

public static class ConfigureServices
{
    public static IServiceCollection AddInfrastructureServices(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        if (configuration.GetValue<bool>("UseInMemoryDatabase"))
            services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase("CASPER"));
        else
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                var connectionString = configuration.GetConnectionString("DefaultConnection");
                var dataSourceBuilder = new NpgsqlDataSourceBuilder(connectionString);
                dataSourceBuilder
                    .MapEnum<BusinessServices>()
                    .MapEnum<TechnologyServices>();

                var dataSource = dataSourceBuilder.Build();
                
                var migrationsAssembly = Assembly.GetExecutingAssembly().FullName;
                options.UseNpgsql(dataSource, builder => builder.MigrationsAssembly(migrationsAssembly));
            });
        }

        services.AddScoped<IApplicationDbContext>(provider => provider.GetRequiredService<ApplicationDbContext>());

        services.AddScoped<ApplicationDbContextInitialiser>();

        services.AddTransient<IDateTime, DateTimeService>();
        services.AddTransient<IIdentityService, IdentityService>();

        return services;
    }
}
