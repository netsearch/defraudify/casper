﻿using CASPER.Application.Common.Interfaces;

namespace CASPER.Infrastructure.Services;

public class DateTimeService : IDateTime
{
    public DateTime Now
        => DateTime.UtcNow;
}
