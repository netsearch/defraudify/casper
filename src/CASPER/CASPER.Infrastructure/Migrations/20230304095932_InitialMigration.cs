﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CASPER.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:business_services", "exchange,custodial,mining")
                .Annotation("Npgsql:Enum:technology_services", "lightning_network,coin_merge");

            migrationBuilder.CreateTable(
                name: "PaymentProviders",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(60)", maxLength: 60, nullable: false),
                    Address = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: false),
                    Website = table.Column<string>(type: "character varying(120)", maxLength: 120, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "NOW() AT TIME ZONE 'UTC'"),
                    LastUpdatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentProviders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VirtualAssetServiceProviders",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(60)", maxLength: 60, nullable: false),
                    Address = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: false),
                    Website = table.Column<string>(type: "character varying(120)", maxLength: 120, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "NOW() AT TIME ZONE 'UTC'"),
                    LastUpdatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VirtualAssetServiceProviders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LegalEntities",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    VirtualAssetServiceProviderId = table.Column<Guid>(type: "uuid", nullable: true),
                    PaymentProviderId = table.Column<Guid>(type: "uuid", nullable: true),
                    Name = table.Column<string>(type: "character varying(80)", maxLength: 80, nullable: false),
                    Website = table.Column<string>(type: "text", nullable: false),
                    GoverningJurisdiction = table.Column<string>(type: "character varying(2)", unicode: false, maxLength: 2, nullable: false),
                    BusinessServices = table.Column<int[]>(type: "integer[]", nullable: false),
                    TechnologyServices = table.Column<int[]>(type: "integer[]", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "NOW() AT TIME ZONE 'UTC'"),
                    LastUpdatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LegalEntities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LegalEntities_PaymentProviders_PaymentProviderId",
                        column: x => x.PaymentProviderId,
                        principalTable: "PaymentProviders",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_LegalEntities_VirtualAssetServiceProviders_VirtualAssetServ~",
                        column: x => x.VirtualAssetServiceProviderId,
                        principalTable: "VirtualAssetServiceProviders",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "PaymentIdentifiers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    VirtualAssetServiceProviderId = table.Column<Guid>(type: "uuid", nullable: false),
                    PaymentProviderId = table.Column<Guid>(type: "uuid", nullable: true),
                    IBAN = table.Column<string>(type: "character varying(34)", maxLength: 34, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "NOW() AT TIME ZONE 'UTC'"),
                    LastUpdatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentIdentifiers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentIdentifiers_PaymentProviders_PaymentProviderId",
                        column: x => x.PaymentProviderId,
                        principalTable: "PaymentProviders",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PaymentIdentifiers_VirtualAssetServiceProviders_VirtualAsse~",
                        column: x => x.VirtualAssetServiceProviderId,
                        principalTable: "VirtualAssetServiceProviders",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "VirtualAssetServiceProviderProperties",
                columns: table => new
                {
                    VirtualAssetServiceProviderId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(60)", maxLength: 60, nullable: false),
                    Content = table.Column<object>(type: "json", maxLength: 300, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "NOW() AT TIME ZONE 'UTC'"),
                    LastUpdatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VirtualAssetServiceProviderProperties", x => new { x.VirtualAssetServiceProviderId, x.Name });
                    table.ForeignKey(
                        name: "FK_VirtualAssetServiceProviderProperties_VirtualAssetServicePr~",
                        column: x => x.VirtualAssetServiceProviderId,
                        principalTable: "VirtualAssetServiceProviders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LegalEntityProperties",
                columns: table => new
                {
                    LegalEntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(60)", maxLength: 60, nullable: false),
                    Content = table.Column<object>(type: "json", maxLength: 300, nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "NOW() AT TIME ZONE 'UTC'"),
                    LastUpdatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LegalEntityProperties", x => new { x.LegalEntityId, x.Name });
                    table.ForeignKey(
                        name: "FK_LegalEntityProperties_LegalEntities_LegalEntityId",
                        column: x => x.LegalEntityId,
                        principalTable: "LegalEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LegalEntities_PaymentProviderId",
                table: "LegalEntities",
                column: "PaymentProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_LegalEntities_VirtualAssetServiceProviderId",
                table: "LegalEntities",
                column: "VirtualAssetServiceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentIdentifiers_PaymentProviderId",
                table: "PaymentIdentifiers",
                column: "PaymentProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentIdentifiers_VirtualAssetServiceProviderId",
                table: "PaymentIdentifiers",
                column: "VirtualAssetServiceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_VirtualAssetServiceProviders_Website",
                table: "VirtualAssetServiceProviders",
                column: "Website",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LegalEntityProperties");

            migrationBuilder.DropTable(
                name: "PaymentIdentifiers");

            migrationBuilder.DropTable(
                name: "VirtualAssetServiceProviderProperties");

            migrationBuilder.DropTable(
                name: "LegalEntities");

            migrationBuilder.DropTable(
                name: "PaymentProviders");

            migrationBuilder.DropTable(
                name: "VirtualAssetServiceProviders");
        }
    }
}
