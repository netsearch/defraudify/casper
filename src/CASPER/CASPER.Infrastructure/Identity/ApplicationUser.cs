﻿using Microsoft.AspNetCore.Identity;

namespace CASPER.Infrastructure.Identity;

public class ApplicationUser : IdentityUser
{
}
