using CASPER.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CASPER.Infrastructure.Converters;

public class CountryIdentifierConverter : ValueConverter<CountryIdentifier, string>
{
    public CountryIdentifierConverter() : base(
        model => model.ToString(),
        provider => new CountryIdentifier(provider),
        new RelationalConverterMappingHints(size: 2, unicode: false)
    )
    {
    }
}
