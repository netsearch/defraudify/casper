﻿using System.Reflection;
using System.Runtime.CompilerServices;
using AutoMapper;
using AutoMapper.EquivalencyExpression;
using FluentValidation;
using CASPER.Application.Common.Behaviours;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Attributes;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace CASPER.Application;

public static class ConfigureServices
{
    public static void ConfigureAutoMapper(IMapperConfigurationExpression config)
    {
        config.AddCollectionMappers();
        config.ShouldMapProperty = property => !property.HasAttribute<AutoMapperIgnoredMemberAttribute>();
        config.ShouldMapField = field => !field.HasAttribute<AutoMapperIgnoredMemberAttribute>()
                                         && !field.HasAttribute<CompilerGeneratedAttribute>();
    }

    public static IServiceCollection AddApplicationServices(this IServiceCollection services)
    {
        services.AddAutoMapper((serviceProvider, config) =>
            {
                ConfigureAutoMapper(config);

                using var serviceScope = serviceProvider.CreateScope();

                var databaseContext = serviceScope.ServiceProvider.GetRequiredService<IApplicationDbContext>();
                config.UseEntityFrameworkCoreModel(databaseContext.Model);
            },
            Assembly.GetExecutingAssembly());

        services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(AuthorizationBehaviour<,>));
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(PerformanceBehaviour<,>));

        return services;
    }
}
