﻿using CASPER.Domain.Common;
using CASPER.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CASPER.Application.Common.Interfaces;

public interface IApplicationDbContext
{
    public IModel Model { get; }

    public DbSet<TEntity> GetDbSet<TEntity>() where TEntity : EntityBase;

    public DbSet<LegalEntity> LegalEntities { get; }

    public DbSet<LegalEntityProperty> LegalEntityProperties { get; }

    public DbSet<PaymentProvider> PaymentProviders { get; }

    public DbSet<PaymentIdentifier> PaymentIdentifiers { get; }

    public DbSet<VirtualAssetServiceProvider> VirtualAssetServiceProviders { get; }

    public DbSet<VirtualAssetServiceProviderProperty> VirtualAssetServiceProviderProperties { get; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}
