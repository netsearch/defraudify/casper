﻿namespace CASPER.Application.Common.Interfaces;

public interface IDateTime
{
    DateTime Now { get; }
}
