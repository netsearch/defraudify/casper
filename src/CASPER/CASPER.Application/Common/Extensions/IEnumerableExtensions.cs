namespace CASPER.Application.Common.Extensions;

// ReSharper disable once InconsistentNaming
public static class IEnumerableExtensions
{
    public static Dictionary<T1, T2> ToDictionary<T1, T2>(this IEnumerable<ValueTuple<T1, T2>> enumerable)
        where T1 : notnull
        => new(enumerable.Select(tuple => new KeyValuePair<T1, T2>(tuple.Item1, tuple.Item2)));

    public static IEnumerable<IEnumerable<T>> Pivot<T>(this IEnumerable<IEnumerable<T>> columns)
    {
        var columnEnumerators = columns.Select(column => column.GetEnumerator()).ToArray();
        try
        {
            while (columnEnumerators.All(columnEnumerator => columnEnumerator.MoveNext()))
                yield return columnEnumerators.Select(columnEnumerator => columnEnumerator.Current).ToArray();
        }
        finally
        {
            Array.ForEach(columnEnumerators, enumerator => enumerator.Dispose());
        }
    }

    public static void ForAll<T>(this IEnumerable<T> enumerable, Action<T> action)
    {
        foreach (var item in enumerable)
            action(item);
    }

    public static IEnumerable<(TSource First, TZip Second)> ZipSelect<TSource, TZip>(
        this IEnumerable<TSource> enumerable,
        Func<TSource, TZip?> zipSelector
    )
        => from source in enumerable
            let zip = zipSelector(source)
            where zip != null
            select (source, zip);

    public static IEnumerable<(TFirst First, TSecond Second)> ZipSelect<TSource, TFirst, TSecond>(
        this IEnumerable<TSource> enumerable,
        Func<TSource, TFirst?> sourceSelector,
        Func<TSource, TSecond?> zipSelector
    )
        => from source in enumerable
            let first = sourceSelector(source)
            let second = zipSelector(source)
            where first != null && second != null
            select (first, second);
}
