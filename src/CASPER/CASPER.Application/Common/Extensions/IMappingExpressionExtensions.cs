using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;
using AutoMapper;

namespace CASPER.Application.Common.Extensions;

// ReSharper disable once InconsistentNaming
public static class IMappingExpressionExtensions
{
    public static IMappingExpression<TSource, TDestination> MapMemberFrom<TSource, TDestination, TMember>(
        this IMappingExpression<TSource, TDestination> @this,
        Expression<Func<TDestination, TMember>> destinationMember,
        Expression<Func<TSource, TMember>> sourceMember
    )
    {
        return @this.ForMember(destinationMember, config => config.MapFrom(sourceMember));
    }

    public static IMappingExpression<TSource, TDestination> IgnoreMember<TSource, TDestination, TMember>(
        this IMappingExpression<TSource, TDestination> @this,
        Expression<Func<TDestination, TMember>> destinationMember
    )
    {
        return @this.ForMember(destinationMember, config => config.Ignore());
    }

    public static IMappingExpression<TSource, TDestination> IgnoreDatabaseGenerated<TSource, TDestination>(
        this IMappingExpression<TSource, TDestination> @this
    )
    {
        foreach (var property in typeof(TDestination).GetProperties())
        {
            if (!property.HasAttribute<DatabaseGeneratedAttribute>())
            {
                continue;
            }

            @this.ForMember(property.Name, config => config.Ignore());
        }

        return @this;
    }


    public static IMappingExpression<TSource, TDestination> DefaultCtorParam<TSource, TDestination, TMember>(
        this IMappingExpression<TSource, TDestination> @this,
        string memberName,
        Expression<Func<TSource, TMember>> @default
    )
    {
        return @this.ForCtorParam(memberName, expr => expr.MapFrom(@default));
    }
}
