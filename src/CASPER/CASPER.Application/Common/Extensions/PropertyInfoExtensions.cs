using System.Reflection;

namespace CASPER.Application.Common.Extensions;

public static class PropertyInfoExtensions
{
    private static readonly NullabilityInfoContext NullabilityContext = new();

    public static bool IsValueNullable(this PropertyInfo @this)
    {
        var nullabilityInfo = NullabilityContext.Create(@this);
        return nullabilityInfo.WriteState == NullabilityState.Nullable;
    }
}
