using System.Linq.Expressions;
using AutoMapper;

namespace CASPER.Application.Common.Extensions;

// ReSharper disable once InconsistentNaming
public static class IProjectionExpressionExtensions
{
    public static IProjectionExpression<TSource, TDestination> IgnoreMember<TSource, TDestination, TMember>(
        this IProjectionExpression<TSource, TDestination> @this,
        Expression<Func<TDestination, TMember>> destinationMember
    )
        => @this.ForMember(destinationMember, config => config.Ignore());
}
