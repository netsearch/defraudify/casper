using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace CASPER.Application.Common.Extensions;

public static class MemberInfoExtensions
{
    public static bool HasAttribute<T>(this MemberInfo memberInfo)
        where T : Attribute
        => Attribute.IsDefined(memberInfo, typeof(T));

    public static bool TryGetAttribute<T>(this MemberInfo memberInfo, [NotNullWhen(true)] out T? attribute)
        where T : Attribute
        => (attribute = memberInfo.GetCustomAttribute<T>()) != null;
}
