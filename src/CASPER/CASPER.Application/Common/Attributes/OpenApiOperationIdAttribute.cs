namespace CASPER.Application.Common.Attributes;

[AttributeUsage(AttributeTargets.Method)]
public class OpenApiOperationIdAttribute : Attribute
{
    public OpenApiOperationIdAttribute(string operationId)
    {
        OperationId = operationId;
    }

    public string OperationId { get; }
}
