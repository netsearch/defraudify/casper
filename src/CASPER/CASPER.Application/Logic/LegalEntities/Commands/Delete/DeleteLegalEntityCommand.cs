﻿using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using MediatR;

namespace CASPER.Application.Logic.LegalEntities.Commands.Delete;

public record DeleteLegalEntityCommand(Guid Id) : IRequest
{
    public class Handler : IRequestHandler<DeleteLegalEntityCommand>
    {
        private readonly IApplicationDbContext _context;

        public Handler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(DeleteLegalEntityCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.LegalEntities
                .FindAsync(new object[] {request.Id}, cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(LegalEntity), request.Id);

            _context.LegalEntities.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
