﻿using FluentValidation;

namespace CASPER.Application.Logic.LegalEntities.Commands.Create;

public class CreateLegalEntityCommandValidator : AbstractValidator<CreateLegalEntityCommand>
{
    public CreateLegalEntityCommandValidator()
    {
        RuleFor(v => v.Name)
            .MaximumLength(80)
            .NotEmpty();

        Unless(v => v.PaymentProviderId.HasValue, () => RuleFor(v => v.VirtualAssetServiceProviderId).NotEmpty());
        Unless(v => v.VirtualAssetServiceProviderId.HasValue, () => RuleFor(v => v.PaymentProviderId).NotEmpty());
    }
}
