﻿using AutoMapper;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Logic.Base.Commands;
using CASPER.Domain.Entities;
using CASPER.Domain.Enums;
using MediatR;

namespace CASPER.Application.Logic.LegalEntities.Commands.Create;

public record CreateLegalEntityCommand(
    Guid? VirtualAssetServiceProviderId,
    Guid? PaymentProviderId,
    string Name,
    Uri Website,
    string GoverningJurisdiction,
    BusinessServices[] BusinessServices,
    TechnologyServices[] TechnologyServices,
    Dictionary<string, object?> Properties
) : IRequest<LegalEntity>
{
    public string GoverningJurisdiction { get; init; } = GoverningJurisdiction.ToUpperInvariant();

    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CreateLegalEntityCommand, LegalEntity>()
                .DefaultCtorParam(nameof(LegalEntity.Id), _ => default(Guid))
                .IgnoreMember(dst => dst.Id)
                .IgnoreDatabaseGenerated();
        }
    }

    public class Handler : CreateCommand<CreateLegalEntityCommand, LegalEntity>.HandlerBase
    {
        public Handler(IApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
