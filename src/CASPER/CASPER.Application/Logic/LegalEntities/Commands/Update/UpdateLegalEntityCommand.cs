﻿using AutoMapper;
using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using CASPER.Domain.Enums;
using MediatR;

namespace CASPER.Application.Logic.LegalEntities.Commands.Update;

public record UpdateLegalEntityCommand(
    Guid Id,
    Guid? VirtualAssetServiceProviderId,
    Guid? PaymentProviderId,
    string Name,
    Uri Website,
    string GoverningJurisdiction,
    BusinessServices[] BusinessServices,
    TechnologyServices[] TechnologyServices,
    Dictionary<string, object?> Properties
) : IRequest
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<UpdateLegalEntityCommand, LegalEntity>()
                .IgnoreMember(dst => dst.Id)
                .IgnoreMember(dst => dst.VirtualAssetServiceProvider)
                .IgnoreDatabaseGenerated();
        }
    }

    public class Handler : IRequestHandler<UpdateLegalEntityCommand>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task Handle(UpdateLegalEntityCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.LegalEntities
                .FindAsync(new object[] {request.Id}, cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(LegalEntity), request.Id);

            _mapper.Map(request, entity);

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
