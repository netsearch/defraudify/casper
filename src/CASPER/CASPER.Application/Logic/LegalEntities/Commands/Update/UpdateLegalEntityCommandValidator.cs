﻿using FluentValidation;

namespace CASPER.Application.Logic.LegalEntities.Commands.Update;

public class UpdateLegalEntityCommandValidator : AbstractValidator<UpdateLegalEntityCommand>
{
    public UpdateLegalEntityCommandValidator()
    {
        RuleFor(v => v.Name)
            .MaximumLength(80)
            .NotEmpty();
        
        Unless(v => v.PaymentProviderId.HasValue, () => RuleFor(v => v.VirtualAssetServiceProviderId).NotEmpty());
        Unless(v => v.VirtualAssetServiceProviderId.HasValue, () => RuleFor(v => v.PaymentProviderId).NotEmpty());
    }
}
