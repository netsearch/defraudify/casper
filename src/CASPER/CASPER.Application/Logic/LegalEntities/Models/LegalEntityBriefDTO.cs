using AutoMapper;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.LegalEntities.Models;

public record LegalEntityBriefDTO(Guid Id, string Name, Uri Website)
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<LegalEntity, LegalEntityBriefDTO>();
        }
    }
}
