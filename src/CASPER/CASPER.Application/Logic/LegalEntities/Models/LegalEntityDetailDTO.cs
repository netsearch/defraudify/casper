using AutoMapper;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Logic.PaymentProviders.Models;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Models;
using CASPER.Domain.Entities;
using CASPER.Domain.Enums;
using CASPER.Domain.ValueObjects;
using Newtonsoft.Json;

namespace CASPER.Application.Logic.LegalEntities.Models;

public record LegalEntityDetailDTO(
    Guid Id,
    string Name,
    Uri Website,
    CountryIdentifier GoverningJurisdiction,
    BusinessServices[] BusinessServices,
    TechnologyServices[] TechnologyServices,
    VirtualAssetServiceProviderBriefDTO? VirtualAssetServiceProvider,
    PaymentProviderBriefDTO? PaymentProvider
)
{
    private List<LegalEntityProperty> _properties = new();

    public Dictionary<string, object?> Properties
        => _properties.ToDictionary(property => property.Name, property => JsonConvert.DeserializeObject<object?>(property.Content as string ?? string.Empty));

    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<LegalEntity, LegalEntityDetailDTO>()
                .MapMemberFrom(dst => dst._properties, src => src.Properties);
        }
    }
}
