﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Common.Mappings;
using CASPER.Application.Common.Models;
using CASPER.Application.Logic.LegalEntities.Models;
using MediatR;

namespace CASPER.Application.Logic.LegalEntities.Queries.GetWithPagination;

public record GetLegalEntitiesWithPaginationQuery(int PageNumber = 1, int PageSize = 20)
    : IRequest<PaginatedList<LegalEntityBriefDTO>>
{
    public class Handler
        : IRequestHandler<GetLegalEntitiesWithPaginationQuery, PaginatedList<LegalEntityBriefDTO>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PaginatedList<LegalEntityBriefDTO>> Handle(
            GetLegalEntitiesWithPaginationQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.LegalEntities
                .OrderBy(dto => dto.Name)
                .ProjectTo<LegalEntityBriefDTO>(_mapper.ConfigurationProvider)
                .PaginatedListAsync(request.PageNumber, request.PageSize);
        }
    }
}
