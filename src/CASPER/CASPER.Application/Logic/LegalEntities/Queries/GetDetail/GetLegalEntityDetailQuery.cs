﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Logic.LegalEntities.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.LegalEntities.Queries.GetDetail;

public record GetLegalEntityDetailQuery(Guid Id) : IRequest<LegalEntityDetailDTO>
{
    public class Handler : IRequestHandler<GetLegalEntityDetailQuery, LegalEntityDetailDTO>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<LegalEntityDetailDTO> Handle(
            GetLegalEntityDetailQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.LegalEntities
                .Where(entity => entity.Id == request.Id)
                .OrderBy(dto => dto.Name)
                .ProjectTo<LegalEntityDetailDTO>(_mapper.ConfigurationProvider)
                .FirstAsync(cancellationToken);
        }
    }
}
