using CASPER.Application.Logic.LegalEntities.Models;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Models;

namespace CASPER.Application.Logic.Risk.Models;

public record RiskReport(
    double Score,
    IEnumerable<string> Issues,
    IEnumerable<VirtualAssetServiceProviderMinimalDTO> VirtualAssetServiceProviders,
    IEnumerable<LegalEntityBriefDTO> LegalEntities
)
{
    public RiskReport(double Score = 0.0)
        : this(Score,
            new List<string>(),
            new List<VirtualAssetServiceProviderMinimalDTO>(),
            new List<LegalEntityBriefDTO>())
    {
    }
}
