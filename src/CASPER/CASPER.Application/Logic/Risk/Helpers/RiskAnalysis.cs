using AutoMapper;
using CASPER.Application.Logic.LegalEntities.Models;
using CASPER.Application.Logic.Risk.Models;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Models;
using CASPER.Domain.Entities;
using CASPER.Domain.Extensions;
using Newtonsoft.Json;

namespace CASPER.Application.Logic.Risk.Helpers;

public static class RiskAnalysis
{
    public static RiskReport CreateRiskReport(IReadOnlyCollection<PaymentIdentifier> paymentIdentifiers, IMapper mapper)
    {
        if (paymentIdentifiers.None())
        {
            return new RiskReport();
        }

        var issueItemCategories = new List<(int Total, int Issues)>();
        var issueDescriptions = new List<string>();

        var legalEntities = paymentIdentifiers
            .SelectMany(x => x.VirtualAssetServiceProvider?.LegalEntities ?? Enumerable.Empty<LegalEntity>())
            .DistinctBy(x => x.Id)
            .ToList();

        (int Total, int Issues) items = (legalEntities.Count, 0);
        foreach (var legalEntity in legalEntities)
        {
            if (legalEntity.Properties.None(x => x.Name is "licenseType" or "licensedBy" && x.Content is not null))
            {
                issueDescriptions.Add($"No license is registered for the related legal entity {legalEntity.Name}");
                items.Issues++;
            }
        }

        issueItemCategories.Add(items);

        var virtualAssetServiceProviders = paymentIdentifiers
            .Select(x => x.VirtualAssetServiceProvider)
            .Where(x => x is not null)
            .DistinctBy(x => x!.Id)
            .Cast<VirtualAssetServiceProvider>()
            .ToList();

        items = (virtualAssetServiceProviders.Count, 0);
        var problematicCrypto = new HashSet<string> {"XMR"};

        foreach (var provider in virtualAssetServiceProviders)
        {
            var cryptoAssetsProp = provider.Properties.SingleOrDefault(x => x.Name is "cryptoAssets");
            var cryptoAssetsContent = cryptoAssetsProp?.Content as string ?? string.Empty;
            var cryptoAssets = JsonConvert.DeserializeObject<HashSet<string>>(cryptoAssetsContent);
            if (cryptoAssets is not null)
            {
                cryptoAssets.IntersectWith(problematicCrypto);
                if (cryptoAssets.Any())
                {
                    issueDescriptions.Add($"Related VASP allows handling of {string.Join(',', problematicCrypto)}");
                    items.Issues++;
                }
            }
        }

        issueItemCategories.Add(items);
        items = (virtualAssetServiceProviders.Count, 0);

        foreach (var provider in virtualAssetServiceProviders)
        {
            var amlPolicy = provider.Properties.SingleOrDefault(x => x.Name is "amlPolicy");
            if (amlPolicy?.Content as string != "true")
            {
                issueDescriptions.Add($"No AML policy is registered for the related VASP {provider.Name}");
                items.Issues++;
            }

            items.Total++;
        }

        issueItemCategories.Add(items);

        var scorePart = 0d;
        var scoreMax = 0d;
        var multiplier = issueItemCategories.Count;
        var multiplierMax = issueItemCategories.Count;
        foreach (var (total, issues) in issueItemCategories)
        {
            scoreMax += 1d * issueItemCategories.Count * multiplierMax--;
            if (issues > 0)
            {
                var categoryPart = (double) issues / total;
                var categoryMax = issueItemCategories.Count * multiplier--;
                scorePart += categoryPart * categoryMax;
            }
        }

        var score = 0.9 * (scorePart / scoreMax);
        var vaspModels = mapper.Map<List<VirtualAssetServiceProviderMinimalDTO>>(virtualAssetServiceProviders);
        var legalEntityModels = mapper.Map<List<LegalEntityBriefDTO>>(legalEntities);
        return new RiskReport(score, issueDescriptions, vaspModels, legalEntityModels);
    }
}
