using AutoMapper;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Logic.Risk.Helpers;
using CASPER.Application.Logic.Risk.Models;
using CASPER.Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.Risk.Queries.GetRiskReport;

public record GetRiskReportQuery(IBAN Iban, DateTime? From = default, DateTime? To = default)
    : IRequest<RiskReport>
{

    public class Handler : IRequestHandler<GetRiskReportQuery, RiskReport>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<RiskReport> Handle(GetRiskReportQuery request, CancellationToken cancellationToken)
        {
            var paymentIdentifiersWithVirtualAssetServiceProviderProperties = _context.PaymentIdentifiers
                .Include(x => x.VirtualAssetServiceProvider!)
                .ThenInclude(x => x.Properties);

            var paymentIdentifierQuery = paymentIdentifiersWithVirtualAssetServiceProviderProperties
                .Include(x => x.VirtualAssetServiceProvider!)
                .ThenInclude(x => x.LegalEntities)
                .ThenInclude(x => x.Properties);

            var paymentIdentifiers = await paymentIdentifierQuery
                .Where(x => x.IBAN == request.Iban)
                .ToListAsync(cancellationToken: cancellationToken);

            return RiskAnalysis.CreateRiskReport(paymentIdentifiers, _mapper);
        }

    }
}
