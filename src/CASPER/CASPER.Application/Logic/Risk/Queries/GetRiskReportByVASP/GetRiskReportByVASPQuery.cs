using AutoMapper;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Logic.Risk.Helpers;
using CASPER.Application.Logic.Risk.Models;
using CASPER.Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.Risk.Queries.GetRiskReportByVASP;

public record GetRiskReportByVaspQuery(
    Guid VirtualAssetServiceProviderId,
    DateTime? From = default,
    DateTime? To = default
) : IRequest<RiskReport>
{
    public class Handler : IRequestHandler<GetRiskReportByVaspQuery, RiskReport>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<RiskReport> Handle(GetRiskReportByVaspQuery request, CancellationToken cancellationToken)
        {
            var paymentIdentifiersWithVirtualAssetServiceProviderProperties = _context.PaymentIdentifiers
                .Include(x => x.VirtualAssetServiceProvider!)
                .ThenInclude(x => x.Properties);

            var paymentIdentifierQuery = paymentIdentifiersWithVirtualAssetServiceProviderProperties
                .Include(x => x.VirtualAssetServiceProvider!)
                .ThenInclude(x => x.LegalEntities)
                .ThenInclude(x => x.Properties);

            var paymentIdentifiers = await paymentIdentifierQuery
                .Where(x => x.VirtualAssetServiceProviderId == request.VirtualAssetServiceProviderId)
                .ToListAsync(cancellationToken: cancellationToken);

            return RiskAnalysis.CreateRiskReport(paymentIdentifiers, _mapper);
        }
    }
}
