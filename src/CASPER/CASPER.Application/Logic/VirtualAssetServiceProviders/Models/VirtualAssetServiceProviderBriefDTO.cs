using AutoMapper;
using CASPER.Application.Logic.PaymentIdentifiers.Models;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Models;

public record VirtualAssetServiceProviderBriefDTO(
    Guid Id,
    string Name,
    Uri Website,
    List<PaymentIdentifierMinimalDTO> PaymentIdentifiers
)
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<VirtualAssetServiceProvider, VirtualAssetServiceProviderBriefDTO>();
        }
    }
}
