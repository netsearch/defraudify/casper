using AutoMapper;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Models;

public record VirtualAssetServiceProviderMinimalDTO(
    Guid Id,
    string Name,
    Uri Website
)
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<VirtualAssetServiceProvider, VirtualAssetServiceProviderMinimalDTO>();
        }
    }
}
