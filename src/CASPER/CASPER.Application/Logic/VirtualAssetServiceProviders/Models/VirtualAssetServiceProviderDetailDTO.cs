using AutoMapper;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Logic.LegalEntities.Models;
using CASPER.Application.Logic.PaymentIdentifiers.Models;
using CASPER.Domain.Entities;
using Newtonsoft.Json;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Models;

public record VirtualAssetServiceProviderDetailDTO(
    Guid Id,
    string Name,
    Uri Website,
    List<LegalEntityBriefDTO> LegalEntities,
    List<PaymentIdentifierBriefDTO> PaymentIdentifiers
)
{
    private List<VirtualAssetServiceProviderProperty> _properties = new();

    public Dictionary<string, object?> Properties
        => _properties.ToDictionary(property => property.Name, property => JsonConvert.DeserializeObject<object?>(property.Content as string ?? ""));

    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<VirtualAssetServiceProvider, VirtualAssetServiceProviderDetailDTO>()
                .MapMemberFrom(dst => dst._properties, src => src.Properties);
        }
    }
}
