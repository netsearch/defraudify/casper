﻿using CASPER.Domain.Events;
using MediatR;
using Microsoft.Extensions.Logging;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.EventHandlers;

public class VirtualAssetServiceProviderCreatedEventHandler : INotificationHandler<VirtualAssetServiceProviderCreatedEvent>
{
    private readonly ILogger<VirtualAssetServiceProviderCreatedEventHandler> _logger;

    public VirtualAssetServiceProviderCreatedEventHandler(
        ILogger<VirtualAssetServiceProviderCreatedEventHandler> logger
    )
    {
        _logger = logger;
    }

    public Task Handle(VirtualAssetServiceProviderCreatedEvent notification, CancellationToken cancellationToken)
    {
        _logger.LogInformation("CASPER.Domain Event: {DomainEvent}", notification.GetType().Name);

        return Task.CompletedTask;
    }
}
