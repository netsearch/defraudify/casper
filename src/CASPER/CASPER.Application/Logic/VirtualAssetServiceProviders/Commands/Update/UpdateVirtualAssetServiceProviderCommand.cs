﻿using AutoMapper;
using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using MediatR;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Commands.Update;

public record UpdateVirtualAssetServiceProviderCommand(
    Guid Id,
    string Name,
    string Address,
    Uri Website,
    List<VirtualAssetServiceProviderProperty> Properties
) : IRequest
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<UpdateVirtualAssetServiceProviderCommand, VirtualAssetServiceProvider>()
                .IgnoreMember(dst => dst.Id)
                .IgnoreMember(dst => dst.LegalEntities)
                .IgnoreMember(dst => dst.PaymentIdentifiers)
                .IgnoreDatabaseGenerated();
        }
    }

    public class Handler : IRequestHandler<UpdateVirtualAssetServiceProviderCommand>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task Handle(UpdateVirtualAssetServiceProviderCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.VirtualAssetServiceProviders
                .FindAsync(new object[] {request.Id}, cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(VirtualAssetServiceProvider), request.Id);

            _mapper.Map(request, entity);

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
