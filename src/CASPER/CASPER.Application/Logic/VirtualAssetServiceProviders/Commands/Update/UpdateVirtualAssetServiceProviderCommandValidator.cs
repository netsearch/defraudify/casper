﻿using FluentValidation;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Commands.Update;

public class UpdateAssetProviderCommandValidator : AbstractValidator<UpdateVirtualAssetServiceProviderCommand>
{
    public UpdateAssetProviderCommandValidator()
    {
        RuleFor(v => v.Address)
            .MaximumLength(200)
            .NotEmpty();
    }
}
