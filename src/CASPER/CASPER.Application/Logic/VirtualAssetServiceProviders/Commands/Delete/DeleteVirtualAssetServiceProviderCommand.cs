﻿using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using MediatR;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Commands.Delete;

public record DeleteVirtualAssetServiceProviderCommand(Guid Id) : IRequest
{
    public class Handler : IRequestHandler<DeleteVirtualAssetServiceProviderCommand>
    {
        private readonly IApplicationDbContext _context;

        public Handler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(DeleteVirtualAssetServiceProviderCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.VirtualAssetServiceProviders
                .FindAsync(new object[] {request.Id}, cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(VirtualAssetServiceProvider), request.Id);

            _context.VirtualAssetServiceProviders.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
