﻿using FluentValidation;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Commands.Create;

public class CreateVirtualAssetServiceProviderCommandValidator
    : AbstractValidator<CreateVirtualAssetServiceProviderCommand>
{
    public CreateVirtualAssetServiceProviderCommandValidator()
    {
        RuleFor(v => v.Address)
            .MaximumLength(200)
            .NotEmpty();
    }
}
