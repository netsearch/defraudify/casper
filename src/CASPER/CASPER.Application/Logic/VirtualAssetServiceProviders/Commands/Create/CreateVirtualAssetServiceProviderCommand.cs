﻿using AutoMapper;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Logic.Base.Commands;
using CASPER.Application.Logic.PaymentIdentifiers.Models;
using CASPER.Domain.Entities;
using MediatR;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Commands.Create;

public record CreateVirtualAssetServiceProviderCommand(
    string Name,
    string Address,
    Uri Website,
    List<PaymentIdentifierAssetProviderDTO> PaymentIdentifiers,
    Dictionary<string, object?> Properties
) : IRequest<VirtualAssetServiceProvider>
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CreateVirtualAssetServiceProviderCommand, VirtualAssetServiceProvider>()
                .DefaultCtorParam(nameof(VirtualAssetServiceProvider.Id), _ => default(Guid))
                .IgnoreMember(dst => dst.Id)
                .IgnoreMember(dst => dst.LegalEntities)
                .IgnoreDatabaseGenerated();
        }
    }

    public class Handler
        : CreateCommand<CreateVirtualAssetServiceProviderCommand, VirtualAssetServiceProvider>.HandlerBase
    {
        public Handler(IApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
