﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Common.Mappings;
using CASPER.Application.Common.Models;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Models;
using MediatR;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Queries.GetWithPagination;

public record GetVirtualAssetServiceProvidersWithPaginationQuery(int PageNumber = 1, int PageSize = 20)
    : IRequest<PaginatedList<VirtualAssetServiceProviderBriefDTO>>
{
    public class Handler
        : IRequestHandler<GetVirtualAssetServiceProvidersWithPaginationQuery,
            PaginatedList<VirtualAssetServiceProviderBriefDTO>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PaginatedList<VirtualAssetServiceProviderBriefDTO>> Handle(
            GetVirtualAssetServiceProvidersWithPaginationQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.VirtualAssetServiceProviders
                .OrderBy(dto => dto.Name)
                .ProjectTo<VirtualAssetServiceProviderBriefDTO>(_mapper.ConfigurationProvider)
                .PaginatedListAsync(request.PageNumber, request.PageSize);
        }
    }
}
