﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.VirtualAssetServiceProviders.Queries.GetDetail;

public record GetVirtualAssetServiceProviderDetailQuery(Guid Id) : IRequest<VirtualAssetServiceProviderDetailDTO>
{
    public class Handler
        : IRequestHandler<GetVirtualAssetServiceProviderDetailQuery, VirtualAssetServiceProviderDetailDTO>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<VirtualAssetServiceProviderDetailDTO> Handle(
            GetVirtualAssetServiceProviderDetailQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.VirtualAssetServiceProviders
                .Where(entity => entity.Id == request.Id)
                .OrderBy(dto => dto.Name)
                .ProjectTo<VirtualAssetServiceProviderDetailDTO>(_mapper.ConfigurationProvider)
                .FirstAsync(cancellationToken);
        }
    }
}
