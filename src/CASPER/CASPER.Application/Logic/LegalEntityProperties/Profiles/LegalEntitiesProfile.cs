using AutoMapper;
using CASPER.Application.Common.Extensions;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.LegalEntityProperties.Profiles;

public class LegalEntityPropertiesProfile : Profile
{
    public LegalEntityPropertiesProfile()
    {
        CreateMap<KeyValuePair<string, object?>, LegalEntityProperty>()
            .ConstructUsing(src => new LegalEntityProperty(Guid.Empty, src.Key, src.Value))
            .MapMemberFrom(dst => dst.Name, src => src.Key)
            .MapMemberFrom(dst => dst.Content, src => src.Value)
            .IgnoreMember(dst => dst.LegalEntityId)
            .IgnoreMember(dst => dst.CreatedAt)
            .IgnoreMember(dst => dst.LastUpdatedAt);
    }
}
