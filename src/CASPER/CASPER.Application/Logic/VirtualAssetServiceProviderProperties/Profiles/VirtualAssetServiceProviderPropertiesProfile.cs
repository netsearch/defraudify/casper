using AutoMapper;
using CASPER.Application.Common.Extensions;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.VirtualAssetServiceProviderProperties.Profiles;

public class VirtualAssetServiceProviderPropertiesProfile : Profile
{
    public VirtualAssetServiceProviderPropertiesProfile()
    {
        CreateMap<KeyValuePair<string, object?>, VirtualAssetServiceProviderProperty>()
            .ConstructUsing(src => new VirtualAssetServiceProviderProperty(Guid.Empty, src.Key, src.Value))
            .MapMemberFrom(dst => dst.Name, src => src.Key)
            .MapMemberFrom(dst => dst.Content, src => src.Value)
            .IgnoreMember(dst => dst.VirtualAssetServiceProviderId)
            .IgnoreMember(dst => dst.CreatedAt)
            .IgnoreMember(dst => dst.LastUpdatedAt);
    }
}
