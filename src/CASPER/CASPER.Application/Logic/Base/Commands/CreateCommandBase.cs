using AutoMapper;
using FluentValidation.Results;
using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.Base.Commands;

public sealed record CreateCommand<TCommand, TResult>
    where TCommand : IRequest<TResult>
    where TResult : EntityBase
{
    public abstract class HandlerBase : IRequestHandler<TCommand, TResult>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        protected HandlerBase(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public virtual async Task<TResult> Handle(TCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<TResult>(request);
            var dbSet = _context.GetDbSet<TResult>();
            await dbSet.AddAsync(entity, cancellationToken);
            //entity.AddDomainEvent(new PaymentIdentifierCreatedEvent(entity));

            try
            {
                await _context.SaveChangesAsync(cancellationToken);
                return entity;
            }
            catch (DbUpdateException e)
            {
                throw new ValidationException(new[]
                {
                    new ValidationFailure(string.Empty, e.InnerException?.Message ?? e.Message),
                });
            }
        }
    }
}
