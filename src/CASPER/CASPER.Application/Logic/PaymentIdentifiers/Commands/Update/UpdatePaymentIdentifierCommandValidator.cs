﻿using System.Text.RegularExpressions;
using FluentValidation;

namespace CASPER.Application.Logic.PaymentIdentifiers.Commands.Update;

public class UpdatePaymentIdentifierCommandValidator : AbstractValidator<UpdatePaymentIdentifierCommand>
{
    public UpdatePaymentIdentifierCommandValidator()
    {
        RuleFor(v => v.IBAN)
            .MaximumLength(40)
            .Matches(new Regex(@"^[A-z]{2}([0-9A-z]\s?){8,38}$"))
            .NotEmpty();
    }
}
