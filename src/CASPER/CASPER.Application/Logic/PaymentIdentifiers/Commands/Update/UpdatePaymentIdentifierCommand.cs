﻿using AutoMapper;
using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using MediatR;

namespace CASPER.Application.Logic.PaymentIdentifiers.Commands.Update;

public record UpdatePaymentIdentifierCommand(Guid Id, string IBAN) : IRequest
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<UpdatePaymentIdentifierCommand, PaymentIdentifier>()
                .IgnoreMember(dst => dst.Id)
                .IgnoreMember(dst => dst.VirtualAssetServiceProviderId)
                .IgnoreMember(dst => dst.VirtualAssetServiceProvider)
                .IgnoreMember(dst => dst.PaymentProviderId)
                .IgnoreMember(dst => dst.PaymentProvider)
                .IgnoreDatabaseGenerated();
        }
    }

    public class Handler : IRequestHandler<UpdatePaymentIdentifierCommand>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task Handle(UpdatePaymentIdentifierCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.PaymentIdentifiers
                .FindAsync(new object[] {request.Id}, cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(PaymentIdentifier), request.Id);

            _mapper.Map(request, entity);

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
