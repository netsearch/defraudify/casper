﻿using System.Text.RegularExpressions;
using FluentValidation;

namespace CASPER.Application.Logic.PaymentIdentifiers.Commands.Create;

public class CreatePaymentIdentifierCommandValidator : AbstractValidator<CreatePaymentIdentifierCommand>
{
    public CreatePaymentIdentifierCommandValidator()
    {
        RuleFor(v => v.IBAN.ToString())
            .MaximumLength(40)
            .Matches(new Regex(@"^[A-z]{2}([0-9A-z]\s?){8,38}$"))
            .NotEmpty();
    }
}
