﻿using AutoMapper;
using FluentValidation.Results;
using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using CASPER.Domain.Events;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.PaymentIdentifiers.Commands.Create;

public record CreatePaymentIdentifierCommand(
    Guid VirtualAssetServiceProviderId,
    Guid? PaymentProviderId,
    string IBAN
) : IRequest<Guid>
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CreatePaymentIdentifierCommand, PaymentIdentifier>()
                .IgnoreMember(dst => dst.Id)
                .IgnoreMember(dst => dst.VirtualAssetServiceProvider)
                .IgnoreMember(dst => dst.PaymentProvider)
                .IgnoreDatabaseGenerated();
        }
    }

    public class Handler : IRequestHandler<CreatePaymentIdentifierCommand, Guid>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Guid> Handle(CreatePaymentIdentifierCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<PaymentIdentifier>(request);
            await _context.PaymentIdentifiers.AddAsync(entity, cancellationToken);
            entity.AddDomainEvent(new PaymentIdentifierCreatedEvent(entity));

            try
            {
                await _context.SaveChangesAsync(cancellationToken);
                return entity.Id;
            }
            catch (DbUpdateException e)
            {
                throw new ValidationException(new[]
                {
                    new ValidationFailure(string.Empty, e.InnerException?.Message ?? e.Message),
                });
            }
        }
    }
}
