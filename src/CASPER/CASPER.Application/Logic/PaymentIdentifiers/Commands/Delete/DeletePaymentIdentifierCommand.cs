﻿using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using MediatR;

namespace CASPER.Application.Logic.PaymentIdentifiers.Commands.Delete;

public record DeletePaymentIdentifierCommand(Guid Id) : IRequest
{
    public class Handler : IRequestHandler<DeletePaymentIdentifierCommand>
    {
        private readonly IApplicationDbContext _context;

        public Handler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(DeletePaymentIdentifierCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.PaymentIdentifiers
                .FindAsync(new object[] {request.Id}, cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(PaymentIdentifier), request.Id);

            _context.PaymentIdentifiers.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
