﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Logic.PaymentIdentifiers.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.PaymentIdentifiers.Queries.GetDetail;

public record GetPaymentIdentifierDetailByIdQuery(Guid Id) : IRequest<PaymentIdentifierDetailDTO>
{
    public class Handler : IRequestHandler<GetPaymentIdentifierDetailByIdQuery, PaymentIdentifierDetailDTO>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PaymentIdentifierDetailDTO> Handle(
            GetPaymentIdentifierDetailByIdQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.PaymentIdentifiers
                .Where(entity => entity.Id == request.Id)
                .Include(dto => dto.VirtualAssetServiceProvider)
                .Include(dto => dto.PaymentProvider)
                .OrderBy(dto => dto.VirtualAssetServiceProvider!.Name)
                .ProjectTo<PaymentIdentifierDetailDTO>(_mapper.ConfigurationProvider)
                .FirstAsync(cancellationToken);
        }
    }
}
