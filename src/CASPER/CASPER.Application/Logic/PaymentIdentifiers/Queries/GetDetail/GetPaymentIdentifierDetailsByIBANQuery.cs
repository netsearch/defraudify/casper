﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Logic.PaymentIdentifiers.Models;
using CASPER.Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.PaymentIdentifiers.Queries.GetDetail;

public record GetPaymentIdentifierDetailsByIBANQuery(IBAN IBAN) : IRequest<List<PaymentIdentifierDetailDTO>>
{
    public class Handler : IRequestHandler<GetPaymentIdentifierDetailsByIBANQuery, List<PaymentIdentifierDetailDTO>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<PaymentIdentifierDetailDTO>> Handle(
            GetPaymentIdentifierDetailsByIBANQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.PaymentIdentifiers
                .Where(entity => entity.IBAN == (string)request.IBAN)
                .Include(dto => dto.VirtualAssetServiceProvider)
                .Include(dto => dto.PaymentProvider)
                .OrderBy(dto => dto.VirtualAssetServiceProvider!.Name)
                .ProjectTo<PaymentIdentifierDetailDTO>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }
    }
}
