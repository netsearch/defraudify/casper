﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Common.Mappings;
using CASPER.Application.Common.Models;
using CASPER.Application.Logic.PaymentIdentifiers.Models;
using MediatR;

namespace CASPER.Application.Logic.PaymentIdentifiers.Queries.GetWithPagination;

public record GetPaymentIdentifiersWithPaginationQuery(int PageNumber = 1, int PageSize = 20)
    : IRequest<PaginatedList<PaymentIdentifierBriefDTO>>
{
    public class Handler
        : IRequestHandler<GetPaymentIdentifiersWithPaginationQuery, PaginatedList<PaymentIdentifierBriefDTO>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PaginatedList<PaymentIdentifierBriefDTO>> Handle(
            GetPaymentIdentifiersWithPaginationQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.PaymentIdentifiers
                .OrderBy(dto => dto.IBAN)
                .ProjectTo<PaymentIdentifierBriefDTO>(_mapper.ConfigurationProvider)
                .PaginatedListAsync(request.PageNumber, request.PageSize);
        }
    }
}
