﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Common.Mappings;
using CASPER.Application.Common.Models;
using CASPER.Application.Logic.PaymentIdentifiers.Models;
using MediatR;

namespace CASPER.Application.Logic.PaymentIdentifiers.Queries.GetWithPagination;

public record GetPaymentIdentifierDetailsWithPaginationQuery(int PageNumber = 1, int PageSize = 20)
    : IRequest<PaginatedList<PaymentIdentifierDetailDTO>>
{
    public class Handler
        : IRequestHandler<GetPaymentIdentifierDetailsWithPaginationQuery, PaginatedList<PaymentIdentifierDetailDTO>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PaginatedList<PaymentIdentifierDetailDTO>> Handle(
            GetPaymentIdentifierDetailsWithPaginationQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.PaymentIdentifiers
                .OrderBy(dto => dto.IBAN)
                .ProjectTo<PaymentIdentifierDetailDTO>(_mapper.ConfigurationProvider)
                .PaginatedListAsync(request.PageNumber, request.PageSize);
        }
    }
}
