using AutoMapper;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.PaymentIdentifiers.Models;

public record PaymentIdentifierBriefDTO(Guid Id, Guid PaymentProviderId, string IBAN)
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<PaymentIdentifier, PaymentIdentifierBriefDTO>();
        }
    }
}
