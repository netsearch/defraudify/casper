using AutoMapper;
using CASPER.Application.Logic.PaymentProviders.Models;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Models;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.PaymentIdentifiers.Models;

public record PaymentIdentifierDetailDTO(
    Guid Id,
    string IBAN,
    VirtualAssetServiceProviderDetailDTO VirtualAssetServiceProvider,
    PaymentProviderDetailDTO PaymentProvider
)
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<PaymentIdentifier, PaymentIdentifierDetailDTO>();
        }
    }
}
