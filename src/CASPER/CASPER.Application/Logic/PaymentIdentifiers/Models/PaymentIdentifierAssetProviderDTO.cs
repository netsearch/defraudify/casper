using AutoMapper;
using CASPER.Application.Common.Extensions;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.PaymentIdentifiers.Models;

public record PaymentIdentifierAssetProviderDTO(Guid PaymentProviderId, string IBAN)
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<PaymentIdentifierAssetProviderDTO, PaymentIdentifier>()
                .IgnoreMember(dst => dst.Id)
                .IgnoreMember(dst => dst.VirtualAssetServiceProviderId)
                .IgnoreMember(dst => dst.VirtualAssetServiceProvider)
                .IgnoreMember(dst => dst.PaymentProvider)
                .IgnoreDatabaseGenerated();
        }
    }
}
