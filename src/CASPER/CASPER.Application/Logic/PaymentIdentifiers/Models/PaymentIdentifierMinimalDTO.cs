using AutoMapper;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.PaymentIdentifiers.Models;

public record PaymentIdentifierMinimalDTO(string IBAN)
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<PaymentIdentifier, PaymentIdentifierMinimalDTO>();
        }
    }
}
