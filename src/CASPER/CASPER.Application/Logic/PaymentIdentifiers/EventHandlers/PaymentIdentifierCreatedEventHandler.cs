﻿using CASPER.Domain.Events;
using MediatR;
using Microsoft.Extensions.Logging;

namespace CASPER.Application.Logic.PaymentIdentifiers.EventHandlers;

public class PaymentIdentifierCreatedEventHandler : INotificationHandler<PaymentIdentifierCreatedEvent>
{
    private readonly ILogger<PaymentIdentifierCreatedEventHandler> _logger;

    public PaymentIdentifierCreatedEventHandler(ILogger<PaymentIdentifierCreatedEventHandler> logger)
    {
        _logger = logger;
    }

    public Task Handle(PaymentIdentifierCreatedEvent notification, CancellationToken cancellationToken)
    {
        _logger.LogInformation("CASPER.Domain Event: {DomainEvent}", notification.GetType().Name);

        return Task.CompletedTask;
    }
}
