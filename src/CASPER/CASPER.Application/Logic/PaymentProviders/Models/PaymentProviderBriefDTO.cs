using AutoMapper;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.PaymentProviders.Models;

public record PaymentProviderBriefDTO(Guid Id, string Name, Uri Website)
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<PaymentProvider, PaymentProviderBriefDTO>();
        }
    }
}
