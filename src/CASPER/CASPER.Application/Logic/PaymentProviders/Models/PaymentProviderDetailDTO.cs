using AutoMapper;
using CASPER.Application.Logic.LegalEntities.Models;
using CASPER.Domain.Entities;

namespace CASPER.Application.Logic.PaymentProviders.Models;

public record PaymentProviderDetailDTO(
    Guid Id,
    string Name,
    Uri Website,
    List<LegalEntityBriefDTO> LegalEntities
)
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<PaymentProvider, PaymentProviderDetailDTO>();
        }
    }
}
