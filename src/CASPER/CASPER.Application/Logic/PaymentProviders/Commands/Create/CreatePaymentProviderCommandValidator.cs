﻿using FluentValidation;

namespace CASPER.Application.Logic.PaymentProviders.Commands.Create;

public class CreatePaymentProviderCommandValidator : AbstractValidator<CreatePaymentProviderCommand>
{
    public CreatePaymentProviderCommandValidator()
    {
        RuleFor(v => v.Address)
            .MaximumLength(200)
            .NotEmpty();
    }
}
