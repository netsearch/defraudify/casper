﻿using AutoMapper;
using FluentValidation.Results;
using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using CASPER.Domain.Events;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.PaymentProviders.Commands.Create;

public record CreatePaymentProviderCommand(
    string Name,
    string Address,
    Uri Website
) : IRequest<Guid>
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CreatePaymentProviderCommand, PaymentProvider>()
                .DefaultCtorParam(nameof(PaymentProvider.Id), _ => default(Guid))
                .IgnoreMember(dst => dst.Id)
                .IgnoreMember(dst => dst.PaymentIdentifiers)
                .IgnoreMember(dst => dst.LegalEntities)
                .IgnoreDatabaseGenerated();
        }
    }

    public class Handler : IRequestHandler<CreatePaymentProviderCommand, Guid>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Guid> Handle(CreatePaymentProviderCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<PaymentProvider>(request);
            await _context.PaymentProviders.AddAsync(entity, cancellationToken);
            entity.AddDomainEvent(new PaymentProviderCreatedEvent(entity));

            try
            {
                await _context.SaveChangesAsync(cancellationToken);
                return entity.Id;
            }
            catch (DbUpdateException e)
            {
                throw new ValidationException(new[]
                {
                    new ValidationFailure(string.Empty, e.InnerException?.Message ?? e.Message),
                });
            }
        }
    }
}
