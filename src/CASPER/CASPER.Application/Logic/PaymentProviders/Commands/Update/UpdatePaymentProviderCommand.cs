﻿using AutoMapper;
using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Extensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using MediatR;

namespace CASPER.Application.Logic.PaymentProviders.Commands.Update;

public record UpdatePaymentProviderCommand(
    Guid Id,
    string Name,
    string Address,
    Uri Website
) : IRequest
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<UpdatePaymentProviderCommand, PaymentProvider>()
                .IgnoreMember(dst => dst.Id)
                .IgnoreMember(dst => dst.PaymentIdentifiers)
                .IgnoreMember(dst => dst.LegalEntities)
                .IgnoreDatabaseGenerated();
        }
    }

    public class Handler : IRequestHandler<UpdatePaymentProviderCommand>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task Handle(UpdatePaymentProviderCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.PaymentProviders
                .FindAsync(new object[] {request.Id}, cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(PaymentProvider), request.Id);

            _mapper.Map(request, entity);

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
