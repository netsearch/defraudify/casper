﻿using FluentValidation;

namespace CASPER.Application.Logic.PaymentProviders.Commands.Update;

public class UpdatePaymentProviderCommandValidator : AbstractValidator<UpdatePaymentProviderCommand>
{
    public UpdatePaymentProviderCommandValidator()
    {
        RuleFor(v => v.Address)
            .MaximumLength(200)
            .NotEmpty();
    }
}
