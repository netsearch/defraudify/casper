﻿using CASPER.Application.Common.Exceptions;
using CASPER.Application.Common.Interfaces;
using CASPER.Domain.Entities;
using MediatR;

namespace CASPER.Application.Logic.PaymentProviders.Commands.Delete;

public record DeletePaymentProviderCommand(Guid Id) : IRequest
{
    public class Handler : IRequestHandler<DeletePaymentProviderCommand>
    {
        private readonly IApplicationDbContext _context;

        public Handler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Handle(DeletePaymentProviderCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.PaymentProviders
                .FindAsync(new object[] {request.Id}, cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(PaymentProvider), request.Id);

            _context.PaymentProviders.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
