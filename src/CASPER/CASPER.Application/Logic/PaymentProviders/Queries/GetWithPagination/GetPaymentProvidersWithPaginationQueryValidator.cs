﻿using FluentValidation;

namespace CASPER.Application.Logic.PaymentProviders.Queries.GetWithPagination;

public class GetPaymentProvidersWithPaginationQueryValidator : AbstractValidator<GetPaymentProvidersWithPaginationQuery>
{
    public GetPaymentProvidersWithPaginationQueryValidator()
    {
        RuleFor(x => x.PageNumber)
            .GreaterThanOrEqualTo(1)
            .WithMessage("PageNumber at least greater than or equal to 1.");

        RuleFor(x => x.PageSize)
            .GreaterThanOrEqualTo(1)
            .WithMessage("PageSize at least greater than or equal to 1.");
    }
}
