﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Common.Mappings;
using CASPER.Application.Common.Models;
using CASPER.Application.Logic.PaymentProviders.Models;
using MediatR;

namespace CASPER.Application.Logic.PaymentProviders.Queries.GetWithPagination;

public record GetPaymentProvidersWithPaginationQuery(int PageNumber = 1, int PageSize = 20)
    : IRequest<PaginatedList<PaymentProviderBriefDTO>>
{
    public class Handler
        : IRequestHandler<GetPaymentProvidersWithPaginationQuery, PaginatedList<PaymentProviderBriefDTO>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PaginatedList<PaymentProviderBriefDTO>> Handle(
            GetPaymentProvidersWithPaginationQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.PaymentProviders
                .OrderBy(dto => dto.Name)
                .ProjectTo<PaymentProviderBriefDTO>(_mapper.ConfigurationProvider)
                .PaginatedListAsync(request.PageNumber, request.PageSize);
        }
    }
}
