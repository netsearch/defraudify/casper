﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CASPER.Application.Common.Interfaces;
using CASPER.Application.Logic.PaymentProviders.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace CASPER.Application.Logic.PaymentProviders.Queries.GetDetail;

public record GetPaymentProviderDetailQuery(Guid Id) : IRequest<PaymentProviderDetailDTO>
{
    public class Handler : IRequestHandler<GetPaymentProviderDetailQuery, PaymentProviderDetailDTO>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public Handler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PaymentProviderDetailDTO> Handle(
            GetPaymentProviderDetailQuery request,
            CancellationToken cancellationToken
        )
        {
            return await _context.PaymentProviders
                .Where(entity => entity.Id == request.Id)
                .OrderBy(dto => dto.Name)
                .ProjectTo<PaymentProviderDetailDTO>(_mapper.ConfigurationProvider)
                .FirstAsync(cancellationToken);
        }
    }
}
