﻿using CASPER.Domain.Events;
using MediatR;
using Microsoft.Extensions.Logging;

namespace CASPER.Application.Logic.PaymentProviders.EventHandlers;

public class PaymentProviderCreatedEventHandler : INotificationHandler<PaymentProviderCreatedEvent>
{
    private readonly ILogger<PaymentProviderCreatedEventHandler> _logger;

    public PaymentProviderCreatedEventHandler(ILogger<PaymentProviderCreatedEventHandler> logger)
    {
        _logger = logger;
    }

    public Task Handle(PaymentProviderCreatedEvent notification, CancellationToken cancellationToken)
    {
        _logger.LogInformation("CASPER.Domain Event: {DomainEvent}", notification.GetType().Name);

        return Task.CompletedTask;
    }
}
