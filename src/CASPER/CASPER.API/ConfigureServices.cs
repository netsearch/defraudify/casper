﻿using FluentValidation.AspNetCore;
using CASPER.API.Conventions;
using CASPER.API.Filters;
using CASPER.API.Services;
using CASPER.Application.Common.Interfaces;
using CASPER.Infrastructure.Identity;
using CASPER.Infrastructure.Persistence;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.OpenApi.Models;

namespace CASPER.API;

public static class ConfigureServices
{
    public const string ApiVersion = "v1.0";

    public static IServiceCollection AddAPIServices(this IServiceCollection services)
    {
        services.AddDatabaseDeveloperPageExceptionFilter();

        services.AddSingleton<ICurrentUserService, CurrentUserService>();

        services.AddHttpContextAccessor();

        services.AddHealthChecks()
            .AddDbContextCheck<ApplicationDbContext>();

        services.AddControllers(options =>
        {
            options.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            options.Filters.Add<ApiExceptionFilterAttribute>();
        });
        services.AddFluentValidationAutoValidation();

        services.AddRazorPages();

        // Customise default API behaviour
        services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });

        services.Configure<RouteOptions>(options =>
        {
            options.LowercaseUrls = true;
            options.LowercaseQueryStrings = true;
        });


        services.AddDefaultIdentity<ApplicationUser>()
            .AddEntityFrameworkStores<ApplicationDbContext>();

        services.AddSwaggerGen(config =>
        {
            config.UseOneOfForPolymorphism();
            config.UseAllOfToExtendReferenceSchemas();

            var docXml = Path.Combine(AppContext.BaseDirectory, "documentation.xml");
            config.IncludeXmlComments(docXml, includeControllerXmlComments: true);

            config.SwaggerDoc(ApiVersion, new OpenApiInfo
            {
                Version = ApiVersion,
                Title = "CASPER API",
                Contact = new OpenApiContact
                {
                    Email = "dolejska@fit.vut.cz",
                    // ReSharper disable once StringLiteralTypo
                    Name = "Daniel Dolejška",
                },
            });
        });
        
        // must be after AddSwaggerGen
        services.AddSwaggerGenNewtonsoftSupport();

        return services;
    }
}
