﻿using CASPER.Application.Common.Models;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Commands.Create;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Commands.Delete;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Commands.Update;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Models;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Queries.GetDetail;
using CASPER.Application.Logic.VirtualAssetServiceProviders.Queries.GetWithPagination;
using Microsoft.AspNetCore.Mvc;

namespace CASPER.API.Controllers.v0100;

[Route("api/v1.0/virtual-asset-service-provider")]
public class VirtualAssetServiceProviderController : ApiControllerBase
{
    /// <summary>
    ///     List existing asset providers including a list of related payment identifiers.
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<PaginatedList<VirtualAssetServiceProviderBriefDTO>>> GetWithPagination(
        [FromQuery]
        GetVirtualAssetServiceProvidersWithPaginationQuery query
    )
        => await Mediator.Send(query);

    /// <summary>
    ///     Get details about a single existing asset provider.
    /// </summary>
    /// <param name="id">UID of existing asset provider record</param>
    /// <returns>Asset Provider details</returns>
    [HttpGet("{id:guid}")]
    public async Task<VirtualAssetServiceProviderDetailDTO> GetDetail(Guid id)
        => await Mediator.Send(new GetVirtualAssetServiceProviderDetailQuery(id));

    /// <summary>
    ///     Create new asset provider record.
    /// </summary>
    /// <param name="command">Information of new asset provider record.</param>
    /// <returns>UID of the created record</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<Guid>> Create(CreateVirtualAssetServiceProviderCommand command)
    {
        var commandResult = await Mediator.Send(command);
        return CreatedAtAction(nameof(GetDetail), new {id = commandResult.Id}, commandResult.Id);
    }

    /// <summary>
    ///     Update existing asset provider record.
    /// </summary>
    /// <param name="id">UID of existing asset provider record</param>
    /// <param name="command">Updated asset provider data</param>
    [HttpPut("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> Update(Guid id, UpdateVirtualAssetServiceProviderCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest();
        }

        await Mediator.Send(command);

        return NoContent();
    }

    /// <summary>
    ///     Remove existing asset provider record.
    /// </summary>
    /// <param name="id">UID of existing asset provider record</param>
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult> Delete(Guid id)
    {
        await Mediator.Send(new DeleteVirtualAssetServiceProviderCommand(id));

        return NoContent();
    }
}
