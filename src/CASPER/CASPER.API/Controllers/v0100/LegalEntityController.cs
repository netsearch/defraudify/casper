﻿using CASPER.Application.Common.Models;
using CASPER.Application.Logic.LegalEntities.Commands.Create;
using CASPER.Application.Logic.LegalEntities.Commands.Delete;
using CASPER.Application.Logic.LegalEntities.Commands.Update;
using CASPER.Application.Logic.LegalEntities.Models;
using CASPER.Application.Logic.LegalEntities.Queries.GetDetail;
using CASPER.Application.Logic.LegalEntities.Queries.GetWithPagination;
using Microsoft.AspNetCore.Mvc;

namespace CASPER.API.Controllers.v0100;

public class LegalEntityController : ApiControllerBase
{
    /// <summary>
    ///     List existing legal entities including a list of related payment identifiers.
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<PaginatedList<LegalEntityBriefDTO>>> GetWithPagination(
        [FromQuery]
        GetLegalEntitiesWithPaginationQuery query
    )
        => await Mediator.Send(query);

    /// <summary>
    ///     Get details about a single existing legal entity.
    /// </summary>
    /// <param name="id">UID of existing legal entity record</param>
    /// <returns>Legal Entity details</returns>
    [HttpGet("{id:guid}")]
    public async Task<LegalEntityDetailDTO> GetDetail(Guid id)
        => await Mediator.Send(new GetLegalEntityDetailQuery(id));

    /// <summary>
    ///     Create new legal entity record.
    /// </summary>
    /// <param name="command">Information of new legal entity record.</param>
    /// <returns>UID of the created record</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<Guid>> Create(CreateLegalEntityCommand command)
    {
        var commandResult = await Mediator.Send(command);
        return CreatedAtAction(nameof(GetDetail), new {id = commandResult.Id}, commandResult.Id);
    }

    /// <summary>
    ///     Update existing legal entity record.
    /// </summary>
    /// <param name="id">UID of existing legal entity record</param>
    /// <param name="command">Updated legal entity data</param>
    [HttpPut("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> Update(Guid id, UpdateLegalEntityCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest();
        }

        await Mediator.Send(command);

        return NoContent();
    }

    /// <summary>
    ///     Remove existing legal entity record.
    /// </summary>
    /// <param name="id">UID of existing legal entity record</param>
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult> Delete(Guid id)
    {
        await Mediator.Send(new DeleteLegalEntityCommand(id));

        return NoContent();
    }
}
