﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CASPER.API.Controllers.v0100;

[ApiController]
[Route("api/v1.0/[controller]")]
public abstract class ApiControllerBase : ControllerBase
{
    private ISender? _mediator;

    protected ISender Mediator
        => _mediator ??= HttpContext.RequestServices.GetRequiredService<ISender>();
}
