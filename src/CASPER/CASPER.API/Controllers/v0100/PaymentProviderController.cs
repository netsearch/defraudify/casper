﻿using CASPER.Application.Common.Models;
using CASPER.Application.Logic.PaymentProviders.Commands.Create;
using CASPER.Application.Logic.PaymentProviders.Commands.Delete;
using CASPER.Application.Logic.PaymentProviders.Commands.Update;
using CASPER.Application.Logic.PaymentProviders.Models;
using CASPER.Application.Logic.PaymentProviders.Queries.GetWithPagination;
using Microsoft.AspNetCore.Mvc;

namespace CASPER.API.Controllers.v0100;

public class PaymentProviderController : ApiControllerBase
{
    /// <summary>
    ///     List existing payment providers including a list of related payment identifiers.
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<PaginatedList<PaymentProviderBriefDTO>>> GetPaymentProvidersWithPagination(
        [FromQuery]
        GetPaymentProvidersWithPaginationQuery query
    )
        => await Mediator.Send(query);

    /// <summary>
    ///     Create new payment provider record.
    /// </summary>
    /// <param name="command">Information of new payment provider record.</param>
    /// <returns>UID of the created record</returns>
    [HttpPost]
    public async Task<ActionResult<Guid>> Create(CreatePaymentProviderCommand command)
        => await Mediator.Send(command);

    /// <summary>
    ///     Update existing payment provider record.
    /// </summary>
    /// <param name="id">UID of existing payment provider record</param>
    /// <param name="command">Updated payment provider data</param>
    [HttpPut("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> Update(Guid id, UpdatePaymentProviderCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest();
        }

        await Mediator.Send(command);

        return NoContent();
    }

    /// <summary>
    ///     Remove existing payment provider record.
    /// </summary>
    /// <param name="id">UID of existing payment provider record</param>
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult> Delete(Guid id)
    {
        await Mediator.Send(new DeletePaymentProviderCommand(id));

        return NoContent();
    }
}
