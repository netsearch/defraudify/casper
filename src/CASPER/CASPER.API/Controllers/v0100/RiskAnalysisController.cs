﻿using CASPER.Application.Logic.Risk.Models;
using CASPER.Application.Logic.Risk.Queries.GetRiskReport;
using CASPER.Application.Logic.Risk.Queries.GetRiskReportByVASP;
using CASPER.Domain.ValueObjects;
using Microsoft.AspNetCore.Mvc;

namespace CASPER.API.Controllers.v0100;

public class RiskAnalysisController : ApiControllerBase
{
    /// <summary>
    ///     Get risk analysis rating for the given <paramref name="iban"/> based on related VASPs.
    /// </summary>
    /// <remarks>The results can be filtered for a period.</remarks>
    /// <param name="iban"></param>
    /// <param name="from">currently not used</param>
    /// <param name="to">currently not used</param>
    /// <returns>a number from 0-1 representing the potential transaction risk</returns>
    [HttpGet("{iban}")]
    public async Task<RiskReport> GetDetailByIban(
        string iban,
        [FromQuery] string? from = null,
        [FromQuery] string? to = null
    )
    {
        DateTime? periodFrom = from is not null ? DateTime.Parse(from) : null;
        DateTime? periodTo = to is not null ? DateTime.Parse(to) : null;

        var query = new GetRiskReportQuery((IBAN) iban, periodFrom, periodTo);
        var riskResult = await Mediator.Send(query);
        return riskResult;
    }

    /// <summary>
    ///     Get risk analysis rating for the given VASPs <paramref name="id"/>.
    /// </summary>
    /// <remarks>The results can be filtered for a period.</remarks>
    /// <param name="id"></param>
    /// <param name="from">currently not used</param>
    /// <param name="to">currently not used</param>
    /// <returns>a number from 0-1 representing the potential transaction risk</returns>
    [HttpGet("by-vasp/{id:guid}")]
    public async Task<RiskReport> GetDetailByIban(
        Guid id,
        [FromQuery] string? from = null,
        [FromQuery] string? to = null
    )
    {
        DateTime? periodFrom = from is not null ? DateTime.Parse(from) : null;
        DateTime? periodTo = to is not null ? DateTime.Parse(to) : null;

        var query = new GetRiskReportByVaspQuery(id, periodFrom, periodTo);
        var riskResult = await Mediator.Send(query);
        return riskResult;
    }
}
