﻿using CASPER.Application.Common.Models;
using CASPER.Application.Logic.PaymentIdentifiers.Commands.Create;
using CASPER.Application.Logic.PaymentIdentifiers.Commands.Delete;
using CASPER.Application.Logic.PaymentIdentifiers.Commands.Update;
using CASPER.Application.Logic.PaymentIdentifiers.Models;
using CASPER.Application.Logic.PaymentIdentifiers.Queries.GetDetail;
using CASPER.Application.Logic.PaymentIdentifiers.Queries.GetWithPagination;
using CASPER.Domain.ValueObjects;
using Microsoft.AspNetCore.Mvc;

namespace CASPER.API.Controllers.v0100;

public class PaymentIdentifierController : ApiControllerBase
{
    /// <summary>
    ///     List existing payment identifiers.
    /// </summary>
    [HttpGet]
    public async Task<ActionResult<PaginatedList<PaymentIdentifierBriefDTO>>> GetWithPagination(
        [FromQuery]
        GetPaymentIdentifiersWithPaginationQuery query
    )
        => await Mediator.Send(query);

    /// <summary>
    ///     List existing payment identifiers including a related payment and virtual asset providers.
    /// </summary>
    [HttpGet("detailed")]
    public async Task<ActionResult<PaginatedList<PaymentIdentifierDetailDTO>>> GetDetailWithPagination(
        [FromQuery]
        GetPaymentIdentifierDetailsWithPaginationQuery query
    )
        => await Mediator.Send(query);

    /// <summary>
    ///     Get details about a single existing payment identifier link by its internal UID.
    /// </summary>
    /// <param name="id">UID of existing payment identifier link record</param>
    /// <returns>payment identifier link details</returns>
    [HttpGet("{id:guid}")]
    public async Task<PaymentIdentifierDetailDTO> GetDetailById(Guid id)
        => await Mediator.Send(new GetPaymentIdentifierDetailByIdQuery(id));

    /// <summary>
    ///     Get details about existing payment identifier links by IBAN.
    /// </summary>
    /// <param name="iban">IBAN of existing payment identifier link record</param>
    /// <returns>payment identifier link details</returns>
    [HttpGet("by-iban/{iban}")]
    public async Task<List<PaymentIdentifierDetailDTO>> GetDetailByIBAN(string iban)
        => await Mediator.Send(new GetPaymentIdentifierDetailsByIBANQuery((IBAN) iban));

    /// <summary>
    ///     Create new payment identifier link record.
    /// </summary>
    /// <param name="command">Information of new payment identifier link record.</param>
    /// <returns>UID of the created record</returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<Guid>> Create(CreatePaymentIdentifierCommand command)
    {
        var commandResult = await Mediator.Send(command);
        return CreatedAtAction(nameof(GetDetailById), new {id = commandResult}, commandResult);
    }

    /// <summary>
    ///     Update existing payment identifier link record.
    /// </summary>
    /// <param name="id">UID of existing payment identifier link record</param>
    /// <param name="command">Updated payment identifier data</param>
    [HttpPut("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> Update(Guid id, UpdatePaymentIdentifierCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest();
        }

        await Mediator.Send(command);

        return NoContent();
    }

    /// <summary>
    ///     Remove existing payment identifier link record.
    /// </summary>
    /// <param name="id">UID of existing payment identifier link record</param>
    [HttpDelete("{id:guid}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult> Delete(Guid id)
    {
        await Mediator.Send(new DeletePaymentIdentifierCommand(id));

        return NoContent();
    }
}
