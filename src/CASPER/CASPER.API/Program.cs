using AutoMapper;
using CASPER.API;
using CASPER.Application;
using CASPER.Infrastructure;
using CASPER.Infrastructure.Persistence;
using ConfigureServices = CASPER.API.ConfigureServices;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services
    .AddApplicationServices()
    .AddInfrastructureServices(builder.Configuration)
    .AddAPIServices();

builder.Configuration
    .AddJsonFile("Properties/Configuration/appsettings.json")
    .AddJsonFile($"Properties/Configuration/appsettings.{builder.Environment.EnvironmentName}.json", true);

var app = builder.Build();

app.Services.GetRequiredService<IMapper>().ConfigurationProvider.AssertConfigurationIsValid();

// Configure the HTTP request pipeline.
if (!app.Environment.IsProduction())
{
    app.UseDeveloperExceptionPage();
    app.UseMigrationsEndPoint();

    // Initialise and seed database
    using var scope = app.Services.CreateScope();
    var initialiser = scope.ServiceProvider.GetRequiredService<ApplicationDbContextInitialiser>();
    await initialiser.InitialiseAsync();
    if (app.Environment.IsDevelopment())
        await initialiser.SeedAsync();
}
else
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHealthChecks("/health");
app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseSwagger(config => { config.RouteTemplate = "/api/{documentName}/_specs/openapi.{json|yaml}"; });
app.UseSwaggerUI(config =>
{
    config.SwaggerEndpoint($"/api/{ConfigureServices.ApiVersion}/_specs/openapi.json", ConfigureServices.ApiVersion);
    config.RoutePrefix = string.Empty;
});

app.UseRouting();

app.UseAuthentication();
//app.UseIdentityServer();
app.UseAuthorization();

app.MapControllers();

app.Run();

// Make the implicit Program class public so test projects can access it
namespace CASPER.API
{
    public class Program
    {
    }
}
