namespace CASPER.Domain.Extensions;

// ReSharper disable once InconsistentNaming
public static class IEnumerableExtensions
{
    public static bool None<T>(this IEnumerable<T> @this)
        => @this.Any() == false;

    public static bool None<T>(this IEnumerable<T> @this, Func<T, bool> predicate)
        => @this.Any(predicate) == false;
}
