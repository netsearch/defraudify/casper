using IbanNet;
using IbanNet.Registry;

namespace CASPER.Domain.ValueObjects;

public class IBAN : ValueObject
{
    private static readonly IbanParser Parser = new(IbanRegistry.Default);

    private readonly Iban _data;

    public IBAN(string content)
    {
        _data = Parser.Parse(content);
    }

    public string Content
        => _data.Bban;

    public static implicit operator string(IBAN iban)
        => iban.ToString();

    public static explicit operator IBAN(string code)
        => new(code);

    public override string ToString()
        => _data.ToString(IbanFormat.Electronic);

    protected override IEnumerable<object> GetEqualityComponents()
        => new[] {_data};
}
