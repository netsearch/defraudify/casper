using System.ComponentModel.DataAnnotations;
using ISO3166;

namespace CASPER.Domain.ValueObjects;

public class CountryIdentifier : ValueObject
{
    private readonly Country _data;

    public CountryIdentifier(string twoLetterCode)
    {
        try
        {
            _data = Country.List.Single(country => country.TwoLetterCode == twoLetterCode);
        }
        catch (InvalidOperationException ex)
        {
            throw new ValidationException($"Unknown country identifier '{twoLetterCode}' by ISO3166.", ex);
        }
    }

    public CountryIdentifier(Country countryData)
    {
        _data = countryData;
    }

    public string Name
        => _data.Name;

    public string Code
        => _data.TwoLetterCode;

    public string ThreeLetterCode
        => _data.ThreeLetterCode;

    public static implicit operator string(CountryIdentifier countryIdentifier)
        => countryIdentifier.ToString();

    public static explicit operator CountryIdentifier(string twoLetterCode)
        => new(twoLetterCode);

    public override string ToString()
        => Code;

    protected override IEnumerable<object> GetEqualityComponents()
        => new[] {_data};
}
