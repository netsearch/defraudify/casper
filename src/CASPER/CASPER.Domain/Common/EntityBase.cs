﻿using System.ComponentModel.DataAnnotations.Schema;
using CASPER.Domain.Attributes;

namespace CASPER.Domain.Common;

public abstract record EntityBase
{
    private readonly List<EventBase> _domainEvents = new();

    [NotMapped]
    [AutoMapperIgnoredMember]
    public IReadOnlyCollection<EventBase> DomainEvents
        => _domainEvents.AsReadOnly();

    public void AddDomainEvent(EventBase domainEvent)
    {
        _domainEvents.Add(domainEvent);
    }

    public void RemoveDomainEvent(EventBase domainEvent)
    {
        _domainEvents.Remove(domainEvent);
    }

    public void ClearDomainEvents()
    {
        _domainEvents.Clear();
    }
}
