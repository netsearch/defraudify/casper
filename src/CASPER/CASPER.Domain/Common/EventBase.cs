﻿using MediatR;

namespace CASPER.Domain.Common;

public abstract record EventBase : INotification;
