
using System.ComponentModel.DataAnnotations.Schema;

namespace CASPER.Domain.Common;

public abstract record AuditableEntityBase : EntityBase
{
    // Attribute required for AutoMapper configuration
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public DateTime CreatedAt { get; init; }

    // Attribute required for AutoMapper configuration
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTime? LastUpdatedAt { get; set; }
}
