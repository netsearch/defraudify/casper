namespace CASPER.Domain.Enums;

public enum BusinessServices
{
    Exchange,
    Custodial,
    Mining,
}
