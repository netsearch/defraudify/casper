namespace CASPER.Domain.Attributes;

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class AutoMapperIgnoredMemberAttribute : Attribute
{
}
