namespace CASPER.Domain.Entities;

public record LegalEntityProperty(
    Guid LegalEntityId,
    string Name,
    object? Content
) : AuditableEntityBase
{
    public LegalEntity? LegalEntity { get; }
}
