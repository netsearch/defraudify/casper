namespace CASPER.Domain.Entities;

public record VirtualAssetServiceProviderProperty(
    Guid VirtualAssetServiceProviderId,
    string Name,
    object? Content
) : AuditableEntityBase
{
    public VirtualAssetServiceProvider? VirtualAssetServiceProvider { get; }
}
