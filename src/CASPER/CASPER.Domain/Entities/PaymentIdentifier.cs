﻿namespace CASPER.Domain.Entities;

public record PaymentIdentifier(
    Guid Id,
    Guid VirtualAssetServiceProviderId,
    Guid? PaymentProviderId,
    string IBAN
) : AuditableEntityBase
{
    public PaymentIdentifier(Guid id, string iban) : this(id, Guid.Empty, null, iban)
    {
    }

    public PaymentIdentifier(string iban) : this(Guid.Empty, Guid.Empty, null, iban)
    {
    }

    public VirtualAssetServiceProvider? VirtualAssetServiceProvider { get; }

    public PaymentProvider? PaymentProvider { get; }
}
