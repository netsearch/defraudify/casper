﻿using CASPER.Domain.Enums;
using CASPER.Domain.ValueObjects;

namespace CASPER.Domain.Entities;

public record LegalEntity(
    Guid Id,
    Guid? VirtualAssetServiceProviderId,
    Guid? PaymentProviderId,
    string Name,
    Uri Website,
    CountryIdentifier GoverningJurisdiction,
    List<BusinessServices> BusinessServices,
    List<TechnologyServices> TechnologyServices
) : AuditableEntityBase
{
    public VirtualAssetServiceProvider? VirtualAssetServiceProvider { get; }

    public PaymentProvider? PaymentProvider { get; }

    public List<LegalEntityProperty> Properties { get; } = new();
}
