﻿namespace CASPER.Domain.Entities;

public record PaymentProvider(
    Guid Id,
    string Name,
    string Address,
    Uri Website
) : AuditableEntityBase
{
    public List<LegalEntity> LegalEntities { get; } = new();

    public List<PaymentIdentifier> PaymentIdentifiers { get; } = new();
}
