namespace CASPER.Domain.Events;

public record VirtualAssetServiceProviderCreatedEvent(VirtualAssetServiceProvider VirtualAssetServiceProvider)
    : EventBase;
