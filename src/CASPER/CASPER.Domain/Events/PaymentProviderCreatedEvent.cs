namespace CASPER.Domain.Events;

public record PaymentProviderCreatedEvent(PaymentProvider PaymentProvider) : EventBase;
