namespace CASPER.Domain.Events;

public record PaymentIdentifierCreatedEvent(PaymentIdentifier PaymentIdentifier) : EventBase;
