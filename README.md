# CASPER
This is the POC application created as part of the DEFRAUDify (ITEA) project. 

## Requirements
The database is the only external dependency of this system.
Deployment using Docker will use the following images by default:

| image                              | name          | description                           |
|------------------------------------|---------------|---------------------------------------|
| `mcr.microsoft.com/dotnet/sdk:7.0` | .NET SDK 7.0  | Direct application runtime dependency |
| `postgres:14`                      | PostgreSQL 14 | External database dependency          |

## Building

### Docker
Default runtime target for Docker is Linux x64 (`linux-x64`).
The default target can be changed by providing a different build argument for `DOTNET_RUNTIME_ID` (e.g.: `linux-arm64` for ARM machines).
Other available options can be found in the [official RID Catalog](https://learn.microsoft.com/en-us/dotnet/core/rid-catalog#using-rids).
The container image can be built by simply using:
```
docker build
```
or
```
cp docker-compose.sampl.yaml docker-compose.yaml
docker compose build
```

### Host
Runtime target should be detected automatically when building on the host system.
Application can be build by the following command:
```
dotnet publish "src/CASPER/CASPER.API/CASPER.API.csproj" --self-contained -c Release -o publish
```

This will place all the relevant binaries and configuration content to the `publish` directory in the CWD.
Destination can be changed via the `-o` option.

## Deployment

### Docker

First, copy the sample Docker configuration files:
```
# Docker Compose file
cp docker-compose.sample.yaml docker-compose.yaml
cp .sample.env .env
```

Provide path to the bound Docker volume storage:
```
sed -i '' "s|DATA_DIR=$|DATA_DIR=$(pwd)/docker/running/data|" .env 
sed -i '' "s|CONFIG_DIR=$|CONFIG_DIR=$(pwd)/docker/running/config|" .env 
```

Then, create configuration for database:
```
cp docker/config/postgres.sample.env docker/config/postgres.env
```

> ❗️ Provide default value for `POSTGRES_PASSWORD` in `docker/config/postgres.env`.
```
sed -i '' "s/POSTGRES_PASSWORD=$/POSTGRES_PASSWORD=<selected password>/" docker/config/postgres.env
```
```
# with generated password
POSTGRES_PASSWORD=$(openssl rand -hex 32)
sed -i '' "s/POSTGRES_PASSWORD=\$/POSTGRES_PASSWORD=${POSTGRES_PASSWORD}/" docker/config/postgres.env
```

Create directory structure for application configuration and copy the defaults there:
```
mkdir -p docker/running/config/casper
mkdir -p docker/running/data/{postgres,casper}
cp src/CASPER/CASPER.API/Properties/Configuration/*.json docker/running/config/casper
```

> ❗️ Provide database password used for `POSTGRES_PASSWORD` to application configuration.
```
sed -i '' "s/Password=[^;]*/Password=<selected password>/" docker/running/config/casper/*.json
```
```
# with generated password (previously)
sed -i '' "s/Password=[^;]*/Password=$POSTGRES_PASSWORD/" docker/running/config/casper/*.json
```

Before starting the application revise configured values for `ASPNETCORE_ENVIRONMENT` and `ASPNETCORE_URLS` environment variables.
These define which application configuration should be used and which host+port will be used by the application within the container, respectively.
Publicly available port can always be changed in the `docker-compose.yaml` `ports` directive.

Finally, start the container using:
```
docker compose up -d
```

### Host
Deployment directly on host will require an external PostgreSQL database already running.

> ❗️ Provide selected database connection configuration to the application.
```
sed -i '' "s/Server=[^;]*/Server=<database host>/" publish/Properties/Configuration/*.json
sed -i '' "s/Port=[^;]*/Port=<database port>/" publish/Properties/Configuration/*.json
sed -i '' "s/Database=[^;]*/Database=<database name>/" publish/Properties/Configuration/*.json
sed -i '' "s/UserId=[^;]*/Database=<database username>/" publish/Properties/Configuration/*.json
sed -i '' "s/Password=[^;]*/Password=<database password>/" publish/Properties/Configuration/*.json
```

Run the application after being built using the publish command previously:
```
cd ./publish
export ASPNETCORE_ENVIRONMENT=Development
export ASPNETCORE_URLS=http://0.0.0.0:12345
./CASPER.API
```

## Usage
OpenAPI Swagger docs for the application's API are available at http://127.0.0.1:5024 by default for Docker deployment and http://127.0.0.1:5000 by default for host deployment.
The docs are by default available only when the app is not running in production environment.
