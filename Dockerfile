﻿FROM mcr.microsoft.com/dotnet/sdk:7.0 AS base
WORKDIR /app

FROM base AS nuget

COPY CASPER.sln nuget.config Directory.Build.props /app/build/
COPY . .
RUN find src -name "*.csproj" | xargs cp --parents --target-directory=/app/build/

FROM base AS build
ARG DOTNET_RUNTIME_ID=linux-x64

COPY --from=nuget /app/build .
RUN dotnet restore -r "$DOTNET_RUNTIME_ID" "/app/src/CASPER/CASPER.API/CASPER.API.csproj"

COPY . .
WORKDIR "/app/src/CASPER/CASPER.API"

ARG CASPER_BUILD_VERSION=0.0.0-undefinded
RUN dotnet publish -r "$DOTNET_RUNTIME_ID" "CASPER.API.csproj" \
    --no-restore --no-self-contained -c Release -o /app/publish -p:Version=$CASPER_BUILD_VERSION

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS publish
EXPOSE 80
WORKDIR /app

COPY --from=build /app/publish .
ENTRYPOINT ["./CASPER.API"]
